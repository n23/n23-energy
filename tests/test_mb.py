#
# N23 Energy - application to process and monitor electrical energy data
#
# Copyright (C) 2019-2022 by Artur Wroblewski <wrobell@riseup.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

"""
Unit tests for Modbus related functions.
"""

from n23energy import mb

import pytest
from unittest import mock

@pytest.fixture
def modbus_data() -> list[int]:
    return list(range(10, 39))

def test_register_start() -> None:
    """
    Test calculation of first Modbus register to be read from a device.
    """
    start, count = mb.register_start(mb.DATA_SOURCE)
    assert 2 == start
    assert 28 == count

def test_getting_register_value(modbus_data: list[int]) -> None:
    """
    Test getting value from a list of register values.
    """
    ds = mb.DATA_SOURCE[0]
    result = mb.register_value(2, ds, modbus_data)
    assert 1.0 == result

    ds = mb.DATA_SOURCE[1]
    result = mb.register_value(2, ds, modbus_data)
    assert 3700 == result

@pytest.mark.asyncio
async def test_read(modbus_data: list[int]) -> None:
    """
    Test reading register data from Modbus device.
    """
    async def f(*args, **kw):  # type: ignore
        rr = mock.MagicMock()
        rr.registers = modbus_data[:]
        return rr

    client = mock.AsyncMock()
    client.read_input_registers = f
    result = await mb.read(client, 2, 28, 1, mb.DATA_SOURCE)

    assert (1.0, 3700) == result

# vim: sw=4:et:ai
