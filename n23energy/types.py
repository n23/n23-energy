#
# N23 Energy - application to process and monitor electrical energy data
#
# Copyright (C) 2019-2022 by Artur Wroblewski <wrobell@riseup.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

"""
N23 Energy basic data types and classes.
"""

import dataclasses as dtc
import enum
#import n23.workflow as n23w
import typing as tp
from collections.abc import AsyncIterator

T = tp.TypeVar('T')
Config: tp.TypeAlias = dict[tp.Any, tp.Any]

# energy record consists of
#
#   power
#       current power measurement
#   total
#       total power measured, possibly since a device was installed
EnergyRecord: tp.TypeAlias = tuple[float, float]

class Driver(enum.Enum):
    """
    Enumeration of N23 Energy device drivers.
    """
    MODBUS_SERIAL = 'modbus-serial'
    STREAM = 'stream'

@dtc.dataclass
class Device(tp.Generic[T]):
    """
    Description of a device reading and processing energy data.

    :var name: Name of a device, i.e. `inverter-1'.
    :var reader: Device data reader.
    :var flow: Device data processing workflow.
    :var interval: Interval at which data is read. Read data as it arrives
        if null.
    """
    name: str
    reader: tp.Callable[[], T] | tp.Callable[[], AsyncIterator[T]]
    flow: tp.Any #n23w.Pipe[T, EnergyRecord]
    interval: int | None=None

@dtc.dataclass
class ModbusSerial:
    name: str
    port: str
    unit: int
    baudrate: int=9600
    interval: int=1

@dtc.dataclass(frozen=True)
class MqttDual:
    name: str
    power: str
    total: str
    is_return: bool=False

# vim: sw=4:et:ai
