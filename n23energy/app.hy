;
; N23 Energy - application to process and monitor electrical energy data
;
; Copyright (C) 2019-2025 by Artur Wroblewski <wrobell@riseup.net>
;
; This program is free software: you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation, either version 3 of the License, or
; (at your option) any later version.
;
; This program is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
;
; You should have received a copy of the GNU General Public License
; along with this program.  If not, see <http://www.gnu.org/licenses/>.
;

(import contextlib [asynccontextmanager]
        dataclasses :as dtc
        functools [partial]
        n23energy.device [mqtt-energy-dual modbus-serial]
        n23energy.types [MqttDual ModbusSerial]
        n23energy.streams [streams-client read-stream])

(setv db (n23-sink-from-uri (n23-config-get "global" "db")))
(setv mqtt-broker (n23-config-get "global" "mqtt-broker"))
(setv streams-broker (n23-config-get "global" "streams-broker"))
(setv stream-output (n23-config-get "global" "stream-output"))
(setv devices (n23-config-get "global" "devices"))

; setup scheduler
(setv scheduler (n23-scheduler))
(setv scheduler.timeout 0.75)

; setup database
(db.add-entity
  "energy"
  ["name" "power" "total"]
  :batch True)

; setup output stream
(setv [ctor-publisher publisher-send]
      (let [publisher None]
          (defn :async [asynccontextmanager] -stream-publisher [broker stream]
              (nonlocal publisher)
              (setv client (streams-client broker))
              (with [:async publisher (client.publisher stream)]
                  (yield)))

          (defn :async -send-data [v]
              (assert (is-not publisher None))
              (await (publisher.send (dtc.asdict v))))

          [-stream-publisher -send-data]))

(setv ctx-publisher (ctor-publisher streams-broker stream-output))

; setup sink
(defn :async write-data [v]
    (await (db.write v))
    (await (publisher-send v)))

(n23-add 1 "db-flush" db.flush)

; setup energy devices
(for [dev devices]
  (match dev
    (ModbusSerial) (await (modbus-serial dev write-data))
    (MqttDual) (mqtt-energy-dual mqtt-broker dev write-data)))

(n23-process db ctx-publisher)

; vim: sw=4:et:ai
