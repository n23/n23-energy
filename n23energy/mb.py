#
# N23 Energy - application to process and monitor electrical energy data
#
# Copyright (C) 2019-2025 by Artur Wroblewski <wrobell@riseup.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

"""
Read data from Modbus devices.
"""

import asyncio
import logging
import operator
import typing as tp
from dataclasses import dataclass
from functools import partial

from pymodbus.client import AsyncModbusSerialClient as AsyncModbusClient

from .types import EnergyRecord

logger = logging.getLogger(__name__)

Transform: tp.TypeAlias = tp.Callable[[int], float]
SolarResult: tp.TypeAlias = EnergyRecord | None
DataSources: tp.TypeAlias = tp.Sequence['DataSource']

mul_100: Transform = partial(operator.mul, 100)
div_10: Transform = lambda v: v / 10

@dataclass(frozen=True)
class DataSource:
    """
    Describes source of data to be read from Modbus device.
    """
    name: str
    register: int  # Modbus device register
    transform: Transform

DATA_SOURCE = (
    # current power solar energy production
    DataSource('power', 2, div_10),

    # total energy production
    DataSource('total', 29, mul_100),
)

async def read(
        client: AsyncModbusClient,
        start: int,
        count: int,
        unit: int,
        data_sources: DataSources,
) -> SolarResult | None:
    """
    Read data from Modbus device registers.

    :param client: Modbus device client.
    :param start: Starting register.
    :param count: Number of registers to read.
    :param unit: Modbus unit identifier.
    :param data_sources: Description of Modbus registers to be read.
    """
    # let's use timeout as reported in the following ticket
    #
    #     https://github.com/pymodbus-dev/pymodbus/issues/385
    #
    # just cancellation from the scheduler does not work, and it will need
    # another ticket it seems
    if not client.connected:
        logger.debug('reconnecting')
        await client.connect()
    task = client.read_input_registers(start, count, slave=unit)
    try:
        rr = await asyncio.wait_for(task, timeout=0.5)
    except asyncio.TimeoutError:
        logger.debug('timeout received')
        return None
    else:
        return tuple(  # type: ignore
            register_value(start, ds, rr.registers) for ds in data_sources
        )

async def connect(port: str, baudrate: int) -> AsyncModbusClient:
    """
    Connect to serial Modbus device.
    """
    client = AsyncModbusClient(
        port=port,
        baudrate=baudrate,
        stopbits=1,
        parity='N',
        bytesize=8,
        timeout=0.75,
    )
    await client.connect()
    return client

def register_value(
        start: int,
        data_source: DataSource,
        values: list[int],
) -> float:
    """
    Get register value from list of register values for given description
    of Modbus register.

    :param start: Starting register.
    :param data_source: Description of Modbus register.
    :param values: List of register values.
    """
    v = values[data_source.register - start]
    return data_source.transform(v)

def register_start(data_sources: DataSources) -> tuple[int, int]:
    """
    For collection of Modbus registers description, get start register and
    number of registers to read.

    :param data_sources: Description of Modbus registers to be read.
    """
    f_key = operator.attrgetter('register')
    start = min(data_sources, key=f_key).register
    max_reg = max(data_sources, key=f_key).register
    return start, max_reg - start + 1

# vim: sw=4:et:ai
