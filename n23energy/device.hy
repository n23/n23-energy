;
; N23 Energy - application to process and monitor electrical energy data
;
; Copyright (C) 2019-2024 by Artur Wroblewski <wrobell@riseup.net>
;
; This program is free software: you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation, either version 3 of the License, or
; (at your option) any later version.
;
; This program is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
;
; You should have received a copy of the GNU General Public License
; along with this program.  If not, see <http://www.gnu.org/licenses/>.
;

(import dataclasses :as dtc
        functools [partial]
        operator :as op
        pymodbus

        n23.app [n23-add]
        n23.pipeline [identity fmap ctor-streams-merge]

        n23energy.streams [streams-client read-stream]
        n23energy [mb])

(require n23.pipeline [n23-> fmap-> ?->])

(defn :async modbus-serial [dev sink]
  (setv client (await (mb.connect dev.port dev.baudrate)))
  (setv [start count] (mb.register_start mb.DATA_SOURCE))
  (setv reader (partial mb.read client start count dev.unit mb.DATA_SOURCE))
  (n23-add 1
           f"energy/{dev.name}"
           reader
           (n23-> (fmap-> ?-> bool))
           sink))

(defn mqtt-energy-dual [uri dev sink]
  (setv client (streams-client uri))

  (defn mqtt-add [topic flip merge]
    ; values received by the pipeline can have two meanings
    ;
    ; - positive number is energy consumed to an electrical grid
    ; - negative number is energy sent to the an electrical grid
    ;
    ; use `flip` and `max` to enable tracking of consumed and sent energy
    ;
    ; - consumed energy is 0, when negative number is received
    ; - sent energy is 0, when positive number is received
    ;
    ; expecting single float value from a mqtt topic
    (setv parse-value (fmap (n23-> bytes.decode
                                   float
                                   flip
                                   (max 0))))

    (n23-add f"aux/{dev.name}/{topic}"
             (partial read-stream client topic)
             (n23-> transform-tuple-time-value
                    parse-value
                    merge)
             sink))

  (setv flip (if dev.is-return op.neg identity))
  (setv [merge-left merge-right] (ctor-streams-merge f"energy/{dev.name}"))

  (mqtt-add dev.power flip merge-left)
  (mqtt-add dev.total identity merge-right))

(defn transform-tuple-time-value [result]
  (setv [ts v] result.value)
  (dtc.replace result :time ts :value v))

; vim:sw=2:et:ai
