#
# N23 Energy - application to process and monitor electrical energy data
#
# Copyright (C) 2019-2022 by Artur Wroblewski <wrobell@riseup.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

"""
Classes and functions for RabbitMQ Streams data sources using RbFly
library.
"""

import logging
import rbfly.streams as rbs
from collections.abc import AsyncIterator
from functools import cache

logger = logging.getLogger(__name__)

@cache
def streams_client(uri: str) -> rbs.StreamsClient:
    """
    Create RabbitMQ Streams client.

    Streams client objects are cached. There shall be only one streams
    client per URI.

    :param uri: RabbitMQ Streams broker URI.
    """
    return rbs.streams_client(uri)

@rbs.connection
async def read_stream(
        client: rbs.StreamsClient, stream: str
) -> AsyncIterator[tuple[float, float]]:
    """
    Read time of message and message data from RabbitMQ stream.

    The streams is bound to a MQTT topic

    - time is stream timestamp for a read message
    - message data is parsed as a float number - power or total measurement

    :param client: RabbitMQ Streams client.
    :param uri: RabbitMQ Streams broker URI.
    """
    async for msg in client.subscribe(stream):
        ts = rbs.get_message_ctx().stream_timestamp
        yield ts, msg  # type: ignore

# vim: sw=4:et:ai
