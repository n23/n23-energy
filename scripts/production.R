#!/usr/bin/env Rscript
#
# N23 Energy - application to process and monitor electrical energy data
#
# Copyright (C) 2019-2022 by Artur Wroblewski <wrobell@riseup.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

#
# Show weekly production and usage of electricity.
#
# NOTE: move into n23-energy-report script in the future
#

library(dplyr)
library(ggplot2)
library(lubridate)
library(RPostgreSQL)
library(urltools)
library(xts)

args = commandArgs(trailingOnly=TRUE)

if (length(args) != 2)
    stop('usage:\n\n    production db output\n\n')

db = url_parse(args[1])
output = args[2]

drv = dbDriver('PostgreSQL')
conn = dbConnect(drv, dbname=db$path, host=db$domain)

data.solar = dbGetQuery(conn, sqlInterpolate(
    conn, "
with solar_daily as (
    select time::date as day, max(today) as value
    from solar
    group by day
)
select
    day,
    avg(value) over (
        order by day rows between 7 preceding and current row
    ) as value
from solar_daily
", tz=Sys.timezone()))

data.solar$source = 'solar'
data.solar$value = data.solar$value / 1000

# read meter data, approximate daily use and calculate rolling mean of
# metered data 
data.meter = read.csv('meter.csv')
data.meter$timestamp = round(as.POSIXct(data.meter$timestamp), unit='hour')
data.meter = xts(data.meter$value, order.by=data.meter$timestamp)
colnames(data.meter) <- c('value')

data.meter.full <- seq(min(index(data.meter)), max(index(data.meter)), by='hours')
data.meter.full <- xts(rep(NA, length(data.meter.full)), order.by=data.meter.full)
data.meter.approx = merge(data.meter.full, data.meter, join='left', fill=na.approx)

idx.days = endpoints(data.meter.approx, on='days')
data.meter.approx = data.meter.approx[idx.days]
data.meter.approx = diff(data.meter.approx)[-1]
data.meter.approx = rollapply(data.meter.approx, width=7, FUN=mean)

data.meter = data.frame(
    day=as.Date(index(data.meter.approx)),
    value=data.meter.approx$value,
    source='meter'
)

# merge solar and meter data
data = rbind(data.meter, data.solar)

p = (
    ggplot(data, aes(day, value, color=source))
    + geom_line()
    + labs(x='Day', y='Energy [kWh]')
    + theme_bw()
)

ggsave(output, p)

# vim: sw=4:et:ai
